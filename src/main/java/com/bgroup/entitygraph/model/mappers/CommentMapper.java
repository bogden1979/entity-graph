package com.bgroup.entitygraph.model.mappers;

import com.bgroup.entitygraph.comments.CommentDto;
import com.bgroup.entitygraph.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    @Mappings({
            @Mapping(source = "comment.post.id", target = "postId"),
            @Mapping(source = "comment.user.id", target = "userId")
    })
    CommentDto toCommentDto(Comment comment);
}
