package com.bgroup.entitygraph.model.mappers;

import com.bgroup.entitygraph.model.Comment;
import com.bgroup.entitygraph.model.Post;
import com.bgroup.entitygraph.posts.PostDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface PostMapper {
    @Mappings({
            @Mapping(source = "post.comments", target = "commentIds", qualifiedByName = "commentIds"),
            @Mapping(source = "post.user.id", target = "userId")
    })
    PostDto toPostDto(Post post);

    @Named("commentIds")
    default List<Long> parentFullName(List<Comment> comments) {
        return comments.stream().map(Comment::getId).collect(Collectors.toList());
    }
}
