package com.bgroup.entitygraph.model.mappers;

import com.bgroup.entitygraph.model.User;
import com.bgroup.entitygraph.users.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto toUserDto(User user);
}
