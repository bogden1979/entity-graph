package com.bgroup.entitygraph.comments;

import com.bgroup.entitygraph.model.mappers.CommentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    public List<CommentDto> getAll() {
        return commentRepository.findAll().stream().map(commentMapper::toCommentDto).collect(Collectors.toList());
    }
}
