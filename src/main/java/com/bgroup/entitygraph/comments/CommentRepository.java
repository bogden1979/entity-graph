package com.bgroup.entitygraph.comments;

import com.bgroup.entitygraph.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
