package com.bgroup.entitygraph.comments;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDto {
    private Long id;
    private String reply;
    private Long postId;
    private Long userId;
}
