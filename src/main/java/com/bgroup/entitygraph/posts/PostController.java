package com.bgroup.entitygraph.posts;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;

    @GetMapping
    public List<PostDto> getAllPostsWithFetch() {
        return postService.getAllPostsWithFetch();
    }

    @GetMapping("/graph")
    public List<PostDto> getAllPostsWithGraph() {
        return postService.getAllPostsWithGraph();
    }
}
