package com.bgroup.entitygraph.posts;

import com.bgroup.entitygraph.model.mappers.PostMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;

    public List<PostDto> getAllPostsWithGraph() {
        return postRepository.findAll().stream().map(postMapper::toPostDto).collect(Collectors.toList());
    }

    public List<PostDto> getAllPostsWithFetch() {
        return postRepository.getAllPostsWithFetch().stream().map(postMapper::toPostDto).collect(Collectors.toList());
    }
}
