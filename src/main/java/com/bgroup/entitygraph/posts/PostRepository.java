package com.bgroup.entitygraph.posts;

import com.bgroup.entitygraph.model.Post;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    @EntityGraph("post-entity-graph-with-comment-users")
    List<Post> findAll();

    @Query("select p from Post p join fetch p.comments")
    List<Post> getAllPostsWithFetch();
}
