package com.bgroup.entitygraph.posts;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PostDto {
    private Long id;
    private String subject;
    private List<Long> commentIds = new ArrayList<>();
    private Long userId;
}
