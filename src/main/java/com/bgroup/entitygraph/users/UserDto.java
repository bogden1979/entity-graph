package com.bgroup.entitygraph.users;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private Long id;
    private String name;
    private Integer age;
}
