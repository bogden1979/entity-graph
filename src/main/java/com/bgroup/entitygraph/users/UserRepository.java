package com.bgroup.entitygraph.users;

import com.bgroup.entitygraph.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
