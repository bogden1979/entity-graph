INSERT INTO users (id, age, name) VALUES (1, 43, 'Monika');
INSERT INTO users (id, age, name) VALUES (2, 18, 'Peter');
INSERT INTO users (id, age, name) VALUES (3, 24, 'Garry');

INSERT INTO posts (id, subject, user_id) VALUES (1, 'Hello World', 1);
INSERT INTO posts (id, subject, user_id) VALUES (2, 'Hello Guys', 2);
INSERT INTO posts (id, subject, user_id) VALUES (3, 'Hello Girls', 3);

INSERT INTO comments (id, reply, post_id, user_id) VALUES (1, 'Nice to meet you', 1, 1);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (2, 'How are you?', 2, 1);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (3, 'Hello', 3, 1);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (4, 'Nice weather today', 1, 1);

INSERT INTO comments (id, reply, post_id, user_id) VALUES (5, 'I am happy', 2, 2);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (6, 'Watching you', 2, 2);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (7, 'Miss you', 1, 2);

INSERT INTO comments (id, reply, post_id, user_id) VALUES (8, 'Do not want to discuss it', 1, 3);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (9, 'Glade to hear it', 2, 3);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (10, 'I am tired', 3, 3);
INSERT INTO comments (id, reply, post_id, user_id) VALUES (11, 'Got stuck', 3, 3);
