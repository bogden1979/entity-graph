package com.bgroup.entitygraph;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/init.sql")
@Ignore
public class EntityGraphApplicationTests {

	@Test
	public void contextLoads() {
	}

}
